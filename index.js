// Bài tập 1: Tính tiền lương nhân viên
/**
 * Đầu vào
 *      đặt tên biến, tìm id thẻ và gán giá trị
 * Các bước xử lý
 *      sử dụng công thức result = luongmotngayValue * songaylamValue;
 * Đầu ra
 *      số tiền ngày lương của nhân viên đã nhập
 */
function tinhluong() {
  var luongmotngayValue = document.getElementById("luongmotngay").value;
  var songaylamValue = document.getElementById("songaylam").value;
  console.log({ luongmotngayValue, songaylamValue });
  var result1 = luongmotngayValue * songaylamValue;
  console.log("result-BT1: ", result1, "vnd");
  document.getElementById("result-BT1").innerHTML = `${result1} vnd`;
}

// Kết thúc bài tập 1

// Bài tập 2: Tính giá trị trung bình
/**
 * Đầu vào
 *    đặt tên biến và gán giá trị cho các biến
 * Các bước xử lý
 *    sử dụng công thức: tính tổng các giá trị và chia 5
 * Đầu ra
 *    result = ...
 */
function tinhtrungbinh() {
  var firstValue = document.getElementById("so-thu-nhat").value;
  var secondValue = document.getElementById("so-thu-hai").value;
  var thirdValue = document.getElementById("so-thu-ba").value;
  var fourthValue = document.getElementById("so-thu-tu").value;
  var fifthValue = document.getElementById("so-thu-nam").value;

  console.log({
    firstValue,
    secondValue,
    thirdValue,
    fourthValue,
    fifthValue,
  });
  var result2 =
    (firstValue * 1 +
      secondValue * 1 +
      thirdValue * 1 +
      fourthValue * 1 +
      fifthValue * 1) /
    5;
  console.log("result-BT2: ", result2);
  document.getElementById("result-BT2").innerHTML = result2;
}

// Kết thúc bài tập 2

// Bài tập 3: Quy đổi tiền
/**
 * Đầu vào
 *    đặt tên biến và gán giá trị
 * Các bước xử lý
 *    b1: tạo hàm quydoi()
 *    b2: đặt tên biến và gán giá trị
 *    b3: dùng công thức  result3 = sotienValue * oneUSD;
 *    b4: dùng new Intl.NumberFormat("vn-VN").format(result3)
 * Đầu ra
 *    result3 = ...
 */
function quydoi() {
  var oneUSD = 23500;
  var sotienValue = document.getElementById("sotien-usd").value;
  console.log({ sotienValue });
  var result3 = sotienValue * oneUSD;
  console.log(
    "result-BT3 = ",
    new Intl.NumberFormat("vn-VN").format(result3),
    "USD"
  );
  document.getElementById("result-BT3").innerHTML = `${new Intl.NumberFormat(
    "vn-VN"
  ).format(result3)} USD`;
}
// Kết thúc bài tập 3

// Bài tập 4: Tính diện tích, chu vi hình chữ nhật
/**
 * Đầu vào
 *    gán giá trị cho 2 biến chieudaiValue và chieurongValue và console.log() ra
 * Các bước xử lý
 *    b1: đặt tên biến là dientich và chu vi
 *    b2: công thức: dienTich = chieudaiValue * chieurongValue
 *               chuVi = (chieudaiValue + chieurongValue) * 2
 * Đầu ra
 *    Diện tích:...
 *    Chu vi:  ...
 */
function tinh() {
  var chieudaiValue = document.getElementById("chieudai").value;
  var chieurongValue = document.getElementById("chieurong").value;
  console.log({ chieudaiValue, chieurongValue });
  var dientich = chieudaiValue * chieurongValue;
  var chuvi = (chieudaiValue * 1 + chieurongValue * 1) * 2;
  console.log("result-BT4: dientich =", dientich, "chuvi =", chuvi);
  document.getElementById(
    "result-BT4"
  ).innerHTML = `Diện tích: ${dientich}, Chu vi: ${chuvi}`;
}
// Kết thúc bài tập 4

// Bài tập 5: Tính tổng 2 ký số
/**
 * Đầu vào
 *    đặt tên biến và gán giá trị
 * Các bước xử lý
 *    b1: Lấy hàng đơn vị và hàng chục
 *    b2: tính tổng hàng chục và hàng đơn vị
 * Đầu ra
 *    result5 = ...
 */
function sumhaiso() {
  var hangchucValue = document.getElementById("hai-chu-so").value;
  var hangdonviValue = document.getElementById("hai-chu-so").value;
  var hangChuc = Math.floor(hangchucValue / 10) % 10;
  var hangDonVi = hangdonviValue % 10;
  console.log({ hangChuc, hangDonVi });
  var result5 = hangChuc + hangDonVi;
  console.log("result-BT5: ", result5);
  document.getElementById("result-BT5").innerHTML = result5;
}
// Kết thúc bài tập 5
